// ==UserScript==
// @name         Dynasty Scans Chapter Caching TEMP
// @version      0.0.1
// @description  Caching chapters so you don't have to reload it everytime you want to read it again.
// @author       ngdangtu
// @namespace    https://userscript.ngdangtu.dev/dynasty-scans.com
// @icon         https://icons.duckduckgo.com/i/dynasty-scans.com.ico
// @supportURL   https://gitlab.com/ndt-browserkit/userscript/-/issues/new
// @updateURL    https://gitlab.com/ndt-browserkit/userscript/-/raw/main/dynasty-scans.com/chapter.js
// @match        *://dynasty-scans.com/chapters/*
// @run-at       document-end
// @resource     style  https://gitlab.com/ndt-browserkit/userscript/-/raw/main/dynasty-scans.com/style/chapter.css
// @inject-into  page
// @grant        unsafeWindow
// @grant        GM_getResourceText
// ==/UserScript==


/** 
 * ⦿ lib-3rd/hypescript
 * LICENSE
 * @see https://gitlab.com/ndt-browserkit/userscript/-/raw/main/lib-3rd/hypescript.js
 */
!function (e) { if ("object" == typeof exports && "undefined" != typeof module) module.exports = e(); else if ("function" == typeof define && define.amd) define([], e); else { ("undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this).h = e() } }((function () { var e, t, n = (e = String.prototype.split, t = void 0 === /()??/.exec("")[1], function (n, r, i) { if ("[object RegExp]" !== Object.prototype.toString.call(r)) return e.call(n, r, i); var o, f, l, s, u = [], a = (r.ignoreCase ? "i" : "") + (r.multiline ? "m" : "") + (r.extended ? "x" : "") + (r.sticky ? "y" : ""), c = 0; for (r = new RegExp(r.source, a + "g"), n += "", t || (o = new RegExp("^" + r.source + "$(?!\\s)", a)), i = void 0 === i ? -1 >>> 0 : i >>> 0; (f = r.exec(n)) && !((l = f.index + f[0].length) > c && (u.push(n.slice(c, f.index)), !t && f.length > 1 && f[0].replace(o, (function () { for (var e = 1; e < arguments.length - 2; e++)void 0 === arguments[e] && (f[e] = void 0) })), f.length > 1 && f.index < n.length && Array.prototype.push.apply(u, f.slice(1)), s = f[0].length, c = l, u.length >= i));)r.lastIndex === f.index && r.lastIndex++; return c === n.length ? !s && r.test("") || u.push("") : u.push(n.slice(c)), u.length > i ? u.slice(0, i) : u }), r = [].indexOf, i = function (e, t) { if (r) return e.indexOf(t); for (var n = 0; n < e.length; ++n)if (e[n] === t) return n; return -1 }; function o(e) { return !!e } var f = function (e) { var t = e.classList; if (t) return t; var n = { add: r, remove: f, contains: l, toggle: function (e) { return l(e) ? (f(e), !1) : (r(e), !0) }, toString: function () { return e.className }, length: 0, item: function (e) { return s()[e] || null } }; return n; function r(e) { var t = s(); i(t, e) > -1 || (t.push(e), u(t)) } function f(e) { var t = s(), n = i(t, e); -1 !== n && (t.splice(n, 1), u(t)) } function l(e) { return i(s(), e) > -1 } function s() { return function (e, t) { for (var n = [], r = 0; r < e.length; r++)t(e[r]) && n.push(e[r]); return n }(e.className.split(" "), o) } function u(t) { var r = t.length; e.className = t.join(" "), n.length = r; for (var i = 0; i < t.length; i++)n[i] = t[i]; delete t[r] } }, l = {}, s = "undefined" == typeof window ? {} : window, u = s.document, a = s.Text; function c() { var e = []; function t() { var t = [].slice.call(arguments), r = null; function i(t) { var o, l, s; if (null == t); else if ("string" == typeof t) r ? r.appendChild(o = u.createTextNode(t)) : (s = n(t, /([\.#]?[^\s#.]+)/), /^\.|#/.test(s[1]) && (r = u.createElement("div")), d(s, (function (e) { var t = e.substring(1, e.length); e && (r ? "." === e[0] ? f(r).add(t) : "#" === e[0] && r.setAttribute("id", t) : r = u.createElement(e)) }))); else if ("number" == typeof t || "boolean" == typeof t || t instanceof Date || t instanceof RegExp) r.appendChild(o = u.createTextNode(t.toString())); else if (l = t, "[object Array]" == Object.prototype.toString.call(l)) d(t, i); else if (p(t)) r.appendChild(o = t); else if (t instanceof a) r.appendChild(o = t); else if ("object" == typeof t) for (var c in t) if ("function" == typeof t[c]) /^on\w+/.test(c) ? function (t, n) { r.addEventListener ? (r.addEventListener(t.substring(2), n[t], !1), e.push((function () { r.removeEventListener(t.substring(2), n[t], !1) }))) : (r.attachEvent(t, n[t]), e.push((function () { r.detachEvent(t, n[t]) }))) }(c, t) : (r[c] = t[c](), e.push(t[c]((function (e) { r[c] = e })))); else if ("style" === c) if ("string" == typeof t[c]) r.style.cssText = t[c]; else for (var h in t[c]) !function (n, i) { if ("function" == typeof i) r.style.setProperty(n, i()), e.push(i((function (e) { r.style.setProperty(n, e) }))); else var o = t[c][n].match(/(.*)\W+!important\W*$/); o ? r.style.setProperty(n, o[1], "important") : r.style.setProperty(n, t[c][n]) }(h, t[c][h]); else if ("attrs" === c) for (var g in t[c]) r.setAttribute(g, t[c][g]); else "data-" === c.substr(0, 5) ? r.setAttribute(c, t[c]) : r[c] = t[c]; else "function" == typeof t && (g = t(), r.appendChild(o = p(g) ? g : u.createTextNode(g)), e.push(t((function (e) { p(e) && o.parentElement ? (o.parentElement.replaceChild(e, o), o = e) : o.textContent = e })))); return o } for (; t.length;)i(t.shift()); return r } return t.cleanup = function () { for (var t = 0; t < e.length; t++)e[t](); e.length = 0 }, t } function p(e) { return e && e.nodeName && e.nodeType } function d(e, t) { if (e.forEach) return e.forEach(t); for (var n = 0; n < e.length; n++)t(e[n], n) } return (l = c()).context = c, l }))


/** ⦿ lib/utility.js */
/**
 * Check if the str is blob URI
 * 
 * @param {string} uri 
 * @returns {boolean} true if the string is Blob URI, otherwise returns false
 */
const is_blob = (uri) => typeof uri === 'string' ? uri.slice(0, 5) === 'blob:' : false

/**
 * Extract width and height from an image source
 * @param {ImageBitmapSource} img_src 
 * @returns {Promise<{width:number,height:number}>}
 */
async function get_imgsz(img_src)
{
    const bmp = await createImageBitmap(img_src)
    const { width, height } = bmp
    bmp.close()
    return { width, height }
}

/**
 * Set attributes to indicated element
 * @param {HTMLElement} el 
 * @param {Record<string, string>} kv 
 */
function set_el_attr(el, kv)
{
    if (!el || !kv) return void console.warn(`Invalid param(s) for fn ${set_el_attr.name}: { el, kv }`, el, kv)

    const attr_ls = Object.entries(kv)
    if (attr_ls.length < 1) return void 0

    for (const [key, value] of attr_ls)
    {
        if (key === 'class')
        {
            Array.isArray(value)
                ? el.classList.add(...value)
                : el.classList.add(value)
            continue
        }
        const [_0, cssvar] = key.match(/^--(.+)/i) ?? []
        if (cssvar)
        {
            el.style.setProperty(`--${cssvar}`, value)
            continue
        }
        el.setAttribute(key, value)
    }
}

/** ⦿ lib/cache.js */
class ConvenientCache
{
    /** @type {string} */
    #ns = undefined

    /** @type {CacheStorage} */
    #cs = undefined

    /** @type {string} */
    #c_name = undefined

    /** @type {Promise<Cache>} */
    #c = undefined

    constructor(ns, cachestorage)
    {
        this.#ns = ns
        this.#cs = cachestorage
    }

    create(...key)
    {
        if (!key) throw new Error(`The method ${this.create.name} requires at least 1 key to create cache.`)

        this.#c_name = this.#ns
        if (key.length === 1)
        {
            this.#c_name += '-' + key[0]
        }
        else
        {
            this.#c_name += '-' + key.shift()
            this.#c_name += '/' + key.join('/')
        }
        this.#c = this.#cs.open(this.#c_name)

        return this
    }

    /**
     * Search and get Response from key, 
     * if it does not exist, attempt to fetch & cache once.
     * 
     * @param {RequestInfo} key 
     * @param {CacheQueryOptions} opt 
     * @returns {Promise<Response|undefined>}
     */
    async lookup(key, opt = null)
    {
        const res = await this.match(key, opt)
        if (res) return res

        await this.add(key)
        return this.match(key, opt)
    }

    /**
     * cache.match() but returns Blob instead of Response
     * 
     * @param {RequestInfo} req 
     * @param {CacheQueryOptions} opt 
     * @returns {Promise<Blob|undefined>}
     */
    async get_blob(req, opt = null)
    {
        const not_req_request = !(req instanceof Request)
        const not_req_str = typeof req !== 'string'
        if (!req || not_req_request && not_req_str) return void 0

        // if Blob URI string
        if (is_blob(req)) return void console.error(`The method ${this.get_blob.name} doesn't accept blob URI!`)

        // if Request
        let res = await this.lookup(req, opt)
        if (!res) return void console.error('Unable to fetch the image: ', req)
        return res.blob()
    }

    drop_current_cache()
    {
        console.log(`Cache [${this.#c_name}] removed!`)
        return this.#cs.delete(this.#c_name)
    }

    /**
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/CacheStorage/match)
     * 
     * @param {RequestInfo} req 
     * @param {CacheQueryOptions} opt 
     * @returns {Promise<Response|undefined>}
     */
    async match(req, opt = null)
    {
        return (await this.#c).match(req, opt)
    }

    /**
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/CacheStorage/add)
     * 
     * @param {RequestInfo} req 
     * @returns {Promise<void>}
     */
    async add(req)
    {
        return (await this.#c).add(req)
    }

    /**
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/CacheStorage/delete)
     * 
     * @param {RequestInfo} req 
     * @param {CacheQueryOptions} opt 
     * @returns {Promise<boolean>}
     */
    async delete(req, opt = null)
    {
        return (await this.#c).delete(req, opt)
    }
}


/** ⦿ DsCache */
class DsCache extends ConvenientCache
{
    constructor(cs)
    {
        super('ds', cs)
    }

    static any(key, cs)
    {
        return new this(cs).create(key)
    }
    static series(key, cs)
    {
        return new this(cs).create('s', key)
    }
    static chapter(key, cs)
    {
        return new this(cs).create('c', key)
    }
}




/** ⦿ Predefined Global Variables */
let is_load = false // true if all chapter's images are loaded.
const uw = unsafeWindow
const doc = uw.document

/** ⦿ Setup Style */
doc.head.innerHTML += `<style>#reader{position:relative;padding:0 120px}#reader .btn-toolbar{padding:0}#reader .btn{user-select:none;padding-block:.4em .3em;line-height:1}#reader .btn[disabled]{pointer-events:none}#reader .btn-mini i{vertical-align:-.3em}#reader .pages-list{right:calc(100% + 5px);display:flex;justify-content:flex-start;flex-direction:column;transition:gap .2s ease;transition-property:right,gap}#reader .pages-list.loaded{right:calc(100% + 10px);gap:10px}#reader .pages-list .page{border-radius:1px;background-size:contain;background-position:top center;background-repeat:no-repeat;transition:.35s ease;transition-property:border-radius,padding-block-start,color,background-color}#reader .pages-list .page.active{border-radius:2px;background-size:contain;background-position:top center;background-repeat:no-repeat}#reader .pages-list a:hover{border-radius:4px;background-size:contain;background-position:top center;background-repeat:no-repeat}#reader #image.thumbnail{--img-pad:4px;box-sizing:border-box;padding:var(--img-pad);height:var(--h);width:var(--w);transition:.35s ease;transition-property:width,height}#reader.noresize #image.thumbnail{height:var(--xh);width:var(--xw)}#image::after{content:'';position:absolute;top:0;left:0;z-index:109;display:block;box-sizing:border-box;padding:var(--img-pad);height:100%;width:100%;background-image:var(--img);background-color:transparent;background-size:contain;background-position:top center;background-origin:content-box;background-repeat:no-repeat}@media (max-width: 767px){#reader{padding:0 10px}#reader .pages-list{display:none}#reader.noresize #image.thumbnail{max-width:unset}}@media (prefers-reduced-motion){#reader #image.thumbnail{transition:none}#reader .pages-list .page{transition:.9s ease;transition-property:color,background-color}}</style>`

/** ⦿ Preparation */
const is_mobile = matchMedia('(max-width: 767px)')
const dc = DsCache.chapter(window.location.pathname.split('/').pop(), caches)
const reader =
{
    pg_ls: new Map(),
    el_self: doc.querySelector('#reader'),
    el_display: doc.querySelector('#image'),
    get_pad: (up_scale) => (is_mobile.matches ? 10 : 120) * up_scale, // responsive #reader{padding-inline}
    get_width: function () { return this.el_self.offsetWidth - this.get_pad(2) },
    save_pg: async function (inx, src)
    {
        if (!src) return false

        const promise_blob = src instanceof Response
            ? src.blob()       // from Response
            : dc.get_blob(src) // from string
        const blob = await promise_blob
        if (!blob) return false

        this.pg_ls.set('pg-' + inx, {
            blob: URL.createObjectURL(blob),
            sz: await get_imgsz(blob),
        })
        return true
    }
}

/** ⦿ Helper Functions */
const no_offline_cache = () => (localStorage.getItem('no_offline_cache') ?? 'true') === 'true' // true if user decides to keep cache
const set_offline_cache = (no_offline = true) => localStorage.setItem('no_offline_cache', no_offline)
function get_id(inx = null)
{
    if (['number', 'string'].includes(typeof inx)) return 'pg-' + inx

    const { hash } = doc.location
    if (!hash) return 'pg-1'
    return hash.toLowerCase() === '#last'
        ? 'pg-' + uw.pages.length
        : hash.replace(/^\#/, 'pg-')
}

/** ⦿ Action Functions */
function resize_img({ width: w, height: h })
{
    const max_width = reader.get_width()
    const max_height = 1200

    if (is_mobile.matches) return {
        w: max_width,
        h: Math.floor(max_width * h / w)
    }

    const xw = Math.min(w, max_width)
    const xh = Math.min(h, max_height)

    const is_portrait = h > w
    return is_portrait
        ? { h: xh, w: Math.floor(xh * w / h) }
        : { w: xw, h: Math.floor(xw * h / w) }
}
async function show_img(inx = null)
{
    const pg = reader.pg_ls.get(get_id(inx))
    if (!pg) return void 0

    is_load && window.stop()
    reader.el_display.querySelector('img')?.remove()

    const { w, h } = resize_img(pg.sz)
    set_el_attr(reader.el_display, {
        '--w': w + 'px',
        '--h': h + 'px',
        '--xw': pg.sz.width + 'px',
        '--xh': pg.sz.height + 'px',
        '--img': `url(${pg.blob})`,
    })
}
async function show_thumb(pg_inx)
{
    if (!pg_inx) return void 0

    const { blob, sz } = reader.pg_ls.get('pg-' + pg_inx)

    const nav_inx = reader.el_display.querySelector('#prev_link') ? pg_inx + 1 : pg_inx // + 1 to avoid the #prev_link item if it exist
    const el = reader.el_display.querySelector(`.pages-list a:nth-of-type(${nav_inx})`)
    el.style.backgroundImage = `url(${blob})`
    el.style.paddingBlockStart = `${el.clientWidth * sz.height / sz.width}px`
}
async function prepare_img(check_only = false)
{
    const ls = uw.pages
    const dl_ls_status = []
    for (let i = 0; i < ls.length; i++)
    {
        const url = window.location.origin + ls[i].image
        const reuse_or_loadnew = check_only ? await dc.match(url) : url
        const status = await reader.save_pg(i + 1, reuse_or_loadnew)

        if (status) show_thumb(i + 1)
        dl_ls_status.push(status)
    }

    is_load = dl_ls_status.reduce((prv, cur) => prv && cur, true)
    if (is_load || check_only) reader
        .el_display.querySelector('.pages-list')
        .classList.add('loaded')
    console.log(check_only ? 'Checking Complete :3' : 'Load Complete :D')
}

/** ⦿ Window Events */
window.addEventListener(
    'beforeunload',
    _ => no_offline_cache() && dc.drop_current_cache()
)

/** ⦿ Component Helpers */
const turn_on = (el) => el.removeAttribute('disabled')
const turn_off = (el) => el.setAttribute('disabled', '')
async function ev_btn_preload(e)
{
    turn_off(e.currentTarget)
    set_offline_cache()
    prepare_img()
}
async function ev_btn_cache(e)
{
    turn_off(e.currentTarget)
    turn_off(el_btn_preload)
    set_offline_cache(false)
    await prepare_img()
    turn_on(el_btn_clear)
}
async function ev_btn_clear(e)
{
    turn_off(e.currentTarget)
    const is_done = await dc.drop_current_cache()
    if (is_done) turn_on(el_btn_cache)
}

/** ⦿ Install Component */
const el_btn_preload = h(
    'a.btn.btn-mini',
    { onclick: ev_btn_preload },
    h('i.icon-download'), 'Preload'
)
const el_btn_cache = h(
    'a.btn.btn-mini',
    { onclick: ev_btn_cache },
    h('i.icon-download-alt'), 'Cache NOW'
)
const el_btn_clear = h(
    'a.btn.btn-mini',
    { onclick: ev_btn_clear },
    h('i.icon-trash'), 'Clear Cache'
)
turn_off(el_btn_clear)
reader.el_self?.prepend(h(
    'menu.btn-toolbar',
    h('li.btn-group', el_btn_preload, el_btn_cache, el_btn_clear)
))

/** ⦿ MAIN */
prepare_img(true).then(_ =>
{
    if (is_load)
    {
        turn_off(el_btn_preload)
        turn_off(el_btn_cache)
        turn_on(el_btn_clear)
    }
    show_img()
})
window.addEventListener('hashchange', show_img)
