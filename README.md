# Userscript for Firefox

For document read more: https://www.tampermonkey.net/documentation.php

## Installation

Since all my userscripts is kept in Gitlab, Tampermonkey won't detect it as userscript. So whatever userscript manager you use, you have to copy the raw file and paste it manually.

## Introduction

### Contribution

Contribution includes (but not limit to): raise questions (reasonable one), report bugs, suggest ideas, submit improve code. All the contribution are welcome, however, please keep in mind this motto: 'be civil... ~~war~~ is not an option'. Anyway, have fun :3

### Repository Structure

```
tmp-dist/        Temporary solution for FireMonkey
lib/             Common Libraries
lib-3rd/         3rd-party Libraries
[example.com]/   Scripts for example.com site
    lib/         Libraries for example.com
    style/       Styles for example.com
prj-[name]/      Scripts for specific projects (available across multiple sites)
    lib/         Library for the project
    style/       Style for the project
```

- `lib/` scripts in this directory are not meant to load directly.
- `tmp-dist/` FireMonkey on my PC is unable to understand `@require` meta field, therefore I have to do manual compiling files. TamperMonkey should work normally with scripts in `[example.com]/*.user.js` && `prj-[name]/*.user.js`.

## Credit

**Project logo**

The Userscript logo is a modification of [file type script](https://iconduck.com/icons/102394/file-type-script) icon in "[ Visual Studio Code Icons](https://github.com/vscode-icons/vscode-icons)" from [Iconduck](https://iconduck.com) website. 