declare type TagName = 'div' | 'span' | 'button' | 'menu' | 'ol' | 'ul' | 'li' | 'hr' | 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'p' | 'a' | 'i' | string

declare interface AttrKv {
    href: string,
    disabled: boolean,
    style: Record<string, string>,
    [key: string]: string,
    [event: `on${string}`]: Function
}

declare type ElemetChildren = string | HTMLElement | null

declare function h(tag: TagName, children: ElemetChildren, attr_kv: AttrKv): HTMLElement
declare function h(tag: TagName, attr_kv: AttrKv, ...children: ElemetChildren[]): HTMLElement