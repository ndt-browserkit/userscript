// ==UserScript==
// @name         hyperhype/hyperscript
// @version      2.0.2
// @author       Dominic Tarr
// @description  Create HyperText with JavaScript, on client or server.
// @namespace    https://github.com/hyperhype/hyperscript
// @supportURL   https://github.com/hyperhype/hyperscript/issues
// @run-at       document-start
// @grant        none
// ==/UserScript==

/**
 * LICENSE
 * 
 * ⦿ HyperScript
 * Copyright (c) 2012 'Dominic Tarr'
 * 
 * Permission is hereby granted, free of charge, 
 * to any person obtaining a copy of this software and 
 * associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, 
 * merge, publish, distribute, sublicense, and/or sell 
 * copies of the Software, and to permit persons to whom 
 * the Software is furnished to do so, 
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice 
 * shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR 
 * ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * 
 * ⦿ Cross-Browser Split
 * Cross-Browser Split 1.1.1
 * Copyright 2007-2012 Steven Levithan <stevenlevithan.com>
 * Available under the MIT License
 * ECMAScript compliant, uniform cross-browser split method
 */

// NOTE 1
// This is not official release, I built it for my personal usage.

// NOTE 2
// Built by browserify as UMD format
// To use it, simply call: h('element-name', 'content', {'attr': 'value'})

!function(e){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=e();else if("function"==typeof define&&define.amd)define([],e);else{("undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this).h=e()}}((function(){var e,t,n=(e=String.prototype.split,t=void 0===/()??/.exec("")[1],function(n,r,i){if("[object RegExp]"!==Object.prototype.toString.call(r))return e.call(n,r,i);var o,f,l,s,u=[],a=(r.ignoreCase?"i":"")+(r.multiline?"m":"")+(r.extended?"x":"")+(r.sticky?"y":""),c=0;for(r=new RegExp(r.source,a+"g"),n+="",t||(o=new RegExp("^"+r.source+"$(?!\\s)",a)),i=void 0===i?-1>>>0:i>>>0;(f=r.exec(n))&&!((l=f.index+f[0].length)>c&&(u.push(n.slice(c,f.index)),!t&&f.length>1&&f[0].replace(o,(function(){for(var e=1;e<arguments.length-2;e++)void 0===arguments[e]&&(f[e]=void 0)})),f.length>1&&f.index<n.length&&Array.prototype.push.apply(u,f.slice(1)),s=f[0].length,c=l,u.length>=i));)r.lastIndex===f.index&&r.lastIndex++;return c===n.length?!s&&r.test("")||u.push(""):u.push(n.slice(c)),u.length>i?u.slice(0,i):u}),r=[].indexOf,i=function(e,t){if(r)return e.indexOf(t);for(var n=0;n<e.length;++n)if(e[n]===t)return n;return-1}; function o(e){return!!e}var f=function(e){var t=e.classList;if(t)return t;var n={add:r,remove:f,contains:l,toggle:function(e){return l(e)?(f(e),!1):(r(e),!0)},toString:function(){return e.className},length:0,item:function(e){return s()[e]||null}};return n;function r(e){var t=s();i(t,e)>-1||(t.push(e),u(t))}function f(e){var t=s(),n=i(t,e);-1!==n&&(t.splice(n,1),u(t))}function l(e){return i(s(),e)>-1}function s(){return function(e,t){for(var n=[],r=0;r<e.length;r++)t(e[r])&&n.push(e[r]);return n}(e.className.split(" "),o)}function u(t){var r=t.length;e.className=t.join(" "),n.length=r;for(var i=0;i<t.length;i++)n[i]=t[i];delete t[r]}},l={},s="undefined"==typeof window?{}:window,u=s.document,a=s.Text;function c(){var e=[];function t(){var t=[].slice.call(arguments),r=null;function i(t){var o,l,s;if(null==t);else if("string"==typeof t)r?r.appendChild(o=u.createTextNode(t)):(s=n(t,/([\.#]?[^\s#.]+)/),/^\.|#/.test(s[1])&&(r=u.createElement("div")),d(s,(function(e){var t=e.substring(1,e.length);e&&(r?"."===e[0]?f(r).add(t):"#"===e[0]&&r.setAttribute("id",t):r=u.createElement(e))})));else if("number"==typeof t||"boolean"==typeof t||t instanceof Date||t instanceof RegExp)r.appendChild(o=u.createTextNode(t.toString()));else if(l=t,"[object Array]"==Object.prototype.toString.call(l))d(t,i);else if(p(t))r.appendChild(o=t);else if(t instanceof a)r.appendChild(o=t);else if("object"==typeof t)for(var c in t)if("function"==typeof t[c])/^on\w+/.test(c)?function(t,n){r.addEventListener?(r.addEventListener(t.substring(2),n[t],!1),e.push((function(){r.removeEventListener(t.substring(2),n[t],!1)}))):(r.attachEvent(t,n[t]),e.push((function(){r.detachEvent(t,n[t])})))}(c,t):(r[c]=t[c](),e.push(t[c]((function(e){r[c]=e}))));else if("style"===c)if("string"==typeof t[c])r.style.cssText=t[c];else for(var h in t[c])!function(n,i){if("function"==typeof i)r.style.setProperty(n,i()),e.push(i((function(e){r.style.setProperty(n,e)})));else var o=t[c][n].match(/(.*)\W+!important\W*$/);o?r.style.setProperty(n,o[1],"important"):r.style.setProperty(n,t[c][n])}(h,t[c][h]);else if("attrs"===c)for(var g in t[c])r.setAttribute(g,t[c][g]);else"data-"===c.substr(0,5)?r.setAttribute(c,t[c]):r[c]=t[c];else"function"==typeof t&&(g=t(),r.appendChild(o=p(g)?g:u.createTextNode(g)),e.push(t((function(e){p(e)&&o.parentElement?(o.parentElement.replaceChild(e,o),o=e):o.textContent=e}))));return o}for(;t.length;)i(t.shift());return r}return t.cleanup=function(){for(var t=0;t<e.length;t++)e[t]();e.length=0},t}function p(e){return e&&e.nodeName&&e.nodeType}function d(e,t){if(e.forEach)return e.forEach(t);for(var n=0;n<e.length;n++)t(e[n],n)}return(l=c()).context=c,l}));