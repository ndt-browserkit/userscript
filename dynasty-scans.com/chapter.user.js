// ==UserScript==
// @name         Dynasty Scans Chapter Caching 
// @version      0.0.2
// @description  Caching chapters so you don't have to reload it everytime you want to read it again.
// @author       ngdangtu
// @namespace    https://userscript.ngdangtu.dev/dynasty-scans.com
// @icon         https://icons.duckduckgo.com/i/dynasty-scans.com.ico
// @supportURL   https://gitlab.com/ndt-browserkit/userscript/-/issues/new
// @updateURL    https://gitlab.com/ndt-browserkit/userscript/-/raw/main/dynasty-scans.com/chapter.user.js
// @match        *://dynasty-scans.com/chapters/*
// @run-at       document-end
// @resource     style  https://gitlab.com/ndt-browserkit/userscript/-/raw/main/dynasty-scans.com/style/chapter.css
// @grant        unsafeWindow
// @grant        GM_getResourceText
// @require      https://gitlab.com/ndt-browserkit/userscript/-/raw/main/lib-3rd/hypescript.js
// @require      https://gitlab.com/ndt-browserkit/userscript/-/raw/main/lib/utility.js
// @require      https://gitlab.com/ndt-browserkit/userscript/-/raw/main/lib/cache.js
// @require      https://gitlab.com/ndt-browserkit/userscript/-/raw/main/dynasty-scans.com/lib/cache.js
// ==/UserScript==

/**
 * @TODO remove this when I have better solution.
 * 
 * Require cache.js in here as a temporary solution
 * as FireMonkey doesn't support nesting meta require yet.
 */

/** ⦿ Predefined Global Variables */
let is_load = false // true if all chapter's images are loaded.
const uw = unsafeWindow
const doc = uw.document
const is_mobile = matchMedia('(max-width: 767px)')

/** ⦿ Setup Style */
doc.head.innerHTML += `<style>${GM_getResourceText('style')}</style>`

/** ⦿ Preparation */
const dc = DsCache.chapter(window.location.pathname.split('/').pop(), caches)
const reader =
{
    pg_ls: new Map(),
    el_self: doc.querySelector('#reader'),
    el_display: doc.querySelector('#image'),
    get_pad: (up_scale) => (is_mobile.matches ? 10 : 120) * up_scale, // responsive #reader{padding-inline}
    get_width: function () { return this.el_self.offsetWidth - this.get_pad(2) },
    save_pg: async function (inx, src)
    {
        if (!src) return false

        const promise_blob = src instanceof Response
            ? src.blob()       // from Response
            : dc.get_blob(src) // from string
        const blob = await promise_blob
        if (!blob) return false

        this.pg_ls.set('pg-' + inx, {
            blob: URL.createObjectURL(blob),
            sz: await get_imgsz(blob),
        })
        return true
    }
}

/** ⦿ Helper Functions */
const no_offline_cache = () => (localStorage.getItem('no_offline_cache') ?? 'true') === 'true' // true if user decides to keep cache
const set_offline_cache = (no_offline = true) => localStorage.setItem('no_offline_cache', no_offline)
function get_id(inx = null)
{
    if (['number', 'string'].includes(typeof inx)) return 'pg-' + inx

    const { hash } = doc.location
    if (!hash) return 'pg-1'
    return hash.toLowerCase() === '#last'
        ? 'pg-' + uw.pages.length
        : hash.replace(/^\#/, 'pg-')
}

/** ⦿ Action Functions */
function resize_img({ width: w, height: h })
{
    const max_width = reader.get_width()
    const max_height = 1200

    if (is_mobile.matches) return {
        w: max_width,
        h: Math.floor(max_width * h / w)
    }

    const xw = Math.min(w, max_width)
    const xh = Math.min(h, max_height)

    const is_portrait = h > w
    return is_portrait
        ? { h: xh, w: Math.floor(xh * w / h) }
        : { w: xw, h: Math.floor(xw * h / w) }
}
async function show_img(inx = null)
{
    const pg = reader.pg_ls.get(get_id(inx))
    if (!pg) return void 0

    is_load && window.stop()
    reader.el_display.querySelector('img')?.remove()

    const { w, h } = resize_img(pg.sz)
    set_el_attr(reader.el_display, {
        '--w': w + 'px',
        '--h': h + 'px',
        '--xw': pg.sz.width + 'px',
        '--xh': pg.sz.height + 'px',
        '--img': `url(${pg.blob})`,
    })
}
async function show_thumb(pg_inx)
{
    if (!pg_inx) return void 0

    const { blob, sz } = reader.pg_ls.get('pg-' + pg_inx)

    const nav_inx = reader.el_display.querySelector('#prev_link') ? pg_inx + 1 : pg_inx // + 1 to avoid the #prev_link item if it exist
    const el = reader.el_display.querySelector(`.pages-list a:nth-of-type(${nav_inx})`)
    el.style.backgroundImage = `url(${blob})`
    el.style.paddingBlockStart = `${el.clientWidth * sz.height / sz.width}px`
}
async function prepare_img(check_only = false)
{
    const ls = uw.pages
    const dl_ls_status = []
    for (let i = 0; i < ls.length; i++)
    {
        const url = window.location.origin + ls[i].image
        const reuse_or_loadnew = check_only ? await dc.match(url) : url
        const status = await reader.save_pg(i + 1, reuse_or_loadnew)

        if (status) show_thumb(i + 1)
        dl_ls_status.push(status)
    }

    is_load = dl_ls_status.reduce((prv, cur) => prv && cur, true)
    if (is_load || check_only) reader
        .el_display.querySelector('.pages-list')
        .classList.add('loaded')
    console.log(check_only ? 'Checking Complete :3' : 'Load Complete :D')
}

/** ⦿ Window Events */
window.addEventListener(
    'beforeunload',
    _ => no_offline_cache() && dc.drop_current_cache()
)

/** ⦿ Component Helpers */
const turn_on = (el) => el.removeAttribute('disabled')
const turn_off = (el) => el.setAttribute('disabled', '')
async function ev_btn_preload(e)
{
    turn_off(e.currentTarget)
    set_offline_cache()
    prepare_img()
}
async function ev_btn_cache(e)
{
    turn_off(e.currentTarget)
    turn_off(el_btn_preload)
    set_offline_cache(false)
    await prepare_img()
    turn_on(el_btn_clear)
}
async function ev_btn_clear(e)
{
    turn_off(e.currentTarget)
    const is_done = await dc.drop_current_cache()
    if (is_done) turn_on(el_btn_cache)
}

/** ⦿ Install Component */
const el_btn_preload = h(
    'a.btn.btn-mini',
    { onclick: ev_btn_preload, title: 'Download chapter for one-time use only' },
    h('i.icon-download'), ' Preload'
)
const el_btn_cache = h(
    'a.btn.btn-mini',
    { onclick: ev_btn_cache, title: 'Download chapter for revisiting (multiple session)' },
    h('i.icon-download-alt'), ' Cache NOW'
)
const el_btn_clear = h(
    'a.btn.btn-mini',
    { onclick: ev_btn_clear },
    h('i.icon-trash'), ' Clear Cache'
)
turn_off(el_btn_clear)
reader.el_self?.prepend(h(
    'menu.btn-toolbar',
    h('li.btn-group', el_btn_preload, el_btn_cache, el_btn_clear)
))

/** ⦿ MAIN */
prepare_img(true).then(_ =>
{
    if (is_load)
    {
        turn_off(el_btn_preload)
        turn_off(el_btn_cache)
        turn_on(el_btn_clear)
    }
    show_img()
})
window.addEventListener('hashchange', show_img)