// ==UserScript==
// @name         Dynasty Scans Cache Handler
// @version      0.0.1
// @description  Cache manga so you don't have to reload it everytime you want to read it again.
// @author       ngdangtu
// @namespace    https://userscript.ngdangtu.dev/dynasty-scans.com
// @match        *://dynasty-scans.com/*
// @icon         https://icons.duckduckgo.com/i/dynasty-scans.com.ico
// @supportURL   https://gitlab.com/ndt-browserkit/userscript/-/issues/new
// @run-at       document-start
// @grant        none
// @require      https://gitlab.com/ndt-browserkit/userscript/-/raw/main/lib/cache.js
// @require      https://gitlab.com/ndt-browserkit/userscript/-/raw/main/lib/utility.js
// ==/UserScript==



class DsCache extends ConvenientCache
{
    constructor(cs)
    {
        super('ds', cs)
    }

    static any(key, cs)
    {
        return new this(cs).create(key)
    }
    static series(key, cs)
    {
        return new this(cs).create('s', key)
    }
    static chapter(key, cs)
    {
        return new this(cs).create('c', key)
    }
}