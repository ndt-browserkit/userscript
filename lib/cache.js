// ==UserScript==
// @name         NDT Convenient Cache
// @version      0.0.1
// @author       ngdangtu
// @description  Expanding feature wrapper for Cache API
// @namespace    https://userscript.ngdangtu.dev
// @icon         https://icons.duckduckgo.com/i/userscript.ngdangtu.dev.ico
// @supportURL   https://gitlab.com/ndt-browserkit/userscript/-/issues/new
// @run-at       document-start
// @grant        none
// ==/UserScript==

class ConvenientCache
{
    /** @type {string} */
    #ns = undefined

    /** @type {CacheStorage} */
    #cs = undefined

    /** @type {string} */
    #c_name = undefined

    /** @type {Promise<Cache>} */
    #c = undefined

    constructor(ns, cachestorage)
    {
        this.#ns = ns
        this.#cs = cachestorage
    }

    create(...key)
    {
        if (!key) throw new Error(`The method ${this.create.name} requires at least 1 key to create cache.`)

        this.#c_name = this.#ns
        if (key.length === 1)
        {
            this.#c_name += '-' + key[0]
        }
        else
        {
            this.#c_name += '-' + key.shift()
            this.#c_name += '/' + key.join('/')
        }
        this.#c = this.#cs.open(this.#c_name)

        return this
    }

    /**
     * Search and get Response from key, 
     * if it does not exist, attempt to fetch & cache once.
     * 
     * @param {RequestInfo} key 
     * @param {CacheQueryOptions} opt 
     * @returns {Promise<Response|undefined>}
     */
    async lookup(key, opt = null)
    {
        const res = await this.match(key, opt)
        if (res) return res

        await this.add(key)
        return this.match(key, opt)
    }

    /**
     * cache.match() but returns Blob instead of Response
     * 
     * @param {RequestInfo} req 
     * @param {CacheQueryOptions} opt 
     * @returns {Promise<Blob|undefined>}
     */
    async get_blob(req, opt = null)
    {
        const not_req_request = !(req instanceof Request)
        const not_req_str = typeof req !== 'string'
        if (!req || not_req_request && not_req_str) return void 0

        // if Blob URI string
        if (is_blob(req)) return void console.error(`The method ${this.get_blob.name} doesn't accept blob URI!`)

        // if Request
        let res = await this.lookup(req, opt)
        if (!res) return void console.error('Unable to fetch the image: ', req)
        return res.blob()
    }

    drop_current_cache()
    {
        console.log(`Cache [${this.#c_name}] removed!`)
        return this.#cs.delete(this.#c_name)
    }

    /**
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/CacheStorage/match)
     * 
     * @param {RequestInfo} req 
     * @param {CacheQueryOptions} opt 
     * @returns {Promise<Response|undefined>}
     */
    async match(req, opt = null)
    {
        return (await this.#c).match(req, opt)
    }

    /**
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/CacheStorage/add)
     * 
     * @param {RequestInfo} req 
     * @returns {Promise<void>}
     */
    async add(req)
    {
        return (await this.#c).add(req)
    }

    /**
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/CacheStorage/delete)
     * 
     * @param {RequestInfo} req 
     * @param {CacheQueryOptions} opt 
     * @returns {Promise<boolean>}
     */
    async delete(req, opt = null)
    {
        return (await this.#c).delete(req, opt)
    }
}