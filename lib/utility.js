// ==UserScript==
// @name         NDT Utilities
// @version      0.0.1
// @author       ngdangtu
// @description  Minor scripts for various purposes yet so small to own itself a file
// @namespace    https://userscript.ngdangtu.dev
// @icon         https://icons.duckduckgo.com/i/userscript.ngdangtu.dev.ico
// @supportURL   https://gitlab.com/ndt-browserkit/userscript/-/issues/new
// @run-at       document-start
// @grant        none
// ==/UserScript==

/**
 * Check if the str is blob URI
 * 
 * @param {string} uri 
 * @returns {boolean} true if the string is Blob URI, otherwise returns false
 */
const is_blob = (uri) => typeof uri === 'string' ? uri.slice(0, 5) === 'blob:' : false

/**
 * Extract width and height from an image source
 * @param {ImageBitmapSource} img_src 
 * @returns {Promise<{width:number,height:number}>}
 */
async function get_imgsz(img_src)
{
    const bmp = await createImageBitmap(img_src)
    const { width, height } = bmp
    bmp.close()
    return { width, height }
}

/**
 * Set attributes to indicated element
 * @param {HTMLElement} el 
 * @param {Record<string, string>} kv 
 */
function set_el_attr(el, kv)
{
    if (!el || !kv) return void console.warn(`Invalid param(s) for fn ${set_el_attr.name}: { el, kv }`, el, kv)

    const attr_ls = Object.entries(kv)
    if (attr_ls.length < 1) return void 0

    for (const [key, value] of attr_ls)
    {
        if (key === 'class')
        {
            Array.isArray(value)
                ? el.classList.add(...value)
                : el.classList.add(value)
            continue
        }
        const [_0, cssvar] = key.match(/^--(.+)/i) ?? []
        if (cssvar)
        {
            el.style.setProperty(`--${cssvar}`, value)
            continue
        }
        el.setAttribute(key, value)
    }
}
